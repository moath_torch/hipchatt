//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func == (lhs: [String: [String]], rhs: [String: [String]]) -> Bool {
    var notequal = false
    
    notequal = lhs.values.count != rhs.values.count || lhs.keys.count != rhs.keys.count
    let lkeys: [String] = lhs.keys.map {$0}
    let rkeys: [String] = rhs.keys.map {$0}
    if lkeys.count == 0 { return false }
    notequal = lkeys.sorted() != rkeys.sorted()
    let lvalues: [String] = lhs[lkeys.first!]!
    let rvalues: [String] = rhs[lkeys.first!]!
    notequal = lvalues.sorted() != rvalues.sorted()
    return !notequal
}
func + (lhs: [String: [String]], rhs: [String: [String]]) -> [String: [String]] {
    let rkeys: [String] = rhs.keys.map {$0}
    var newDictionary = lhs
     rkeys.forEach {newDictionary[$0] = rhs[$0]}
     return newDictionary
}



/***FIRST*** The Logic (considering a single mention)
 1. get the whole string to parse
 2. validate (not needed now)
 3. find @
 4. find the first non.alphanumeric value after it
 5. make a range without the @
 6. strip the text of the range from the original string
 7. add it to mentions array
 */
func makeemotionsjson(from str: String) -> [String: [String]] {
    
    return buildMentionsDictionary(message: str)
    //1. validate
    let emptyresponse: [String: [String]]  = ["mentions":[]]
    if str.characters.count == 0 { return emptyresponse}
    let specialChar = "@"
    // find range of the first @ sign
    guard let _r = str.range(of: specialChar)?.lowerBound else {
        // no mention signs found
        return emptyresponse
    }
    // get the whole string starting frmo @ sign
    let sub = str.substring(from: _r)
    // find next alphanum char
    guard let allalphanumRange = sub.rangeOfCharacter(from: CharacterSet.alphanumerics) else {
        // no alpha num chars next (only@ sign or @ occurs at the end)
        return emptyresponse
    }
    // get the string without @ sign
    let withoutAt = sub.substring(from: allalphanumRange.lowerBound)
    // make sure @ sign is alone (cover case 6)
    guard sub.substring(to: allalphanumRange.lowerBound).characters.count == 1 else {
        return emptyresponse
    }
    // find next non alphanumeric value
    let nonalpha = withoutAt.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted)
    if nonalpha != nil {
        let onlyname = withoutAt.substring(to: nonalpha!.lowerBound)
        print(onlyname)
        return ["mentions":[onlyname]]
    } else {
        print(withoutAt)
        // we coud not find any non-alpha numeric values, returns the name without @ sign #5
        return ["mentions":[withoutAt]]
    }
    
}



func findmatches(in message: String, patter: String, leftInset: Int, rightInset: Int) -> [String] {
    let regex = try? NSRegularExpression(pattern: patter, options: NSRegularExpression.Options.caseInsensitive)
    let matches = regex?.matches(in: message, options: [], range: NSRange(location: 0, length: message.characters.count))
    var results: [String] = []
    for match in matches! {
        let range =  match.rangeAt(0)
        let tr = message.index(message.startIndex, offsetBy: range.location + leftInset) ..< message.index(message.startIndex, offsetBy: range.location + range.length - rightInset)
        results.append(message.substring(with: tr))
    }
    return results
}

func buildMentionsDictionary(message: String) -> [String: [String]] {
    // first valid mention @ sign is a sign that is followed by an alphanumeric value
    let usersMentioned: [String] = findmatches(in: message, patter: "@[a-z0-9A-Z]+", leftInset: 1, rightInset: 0)
    return ["mentions": usersMentioned]
}

func buildEmoticonsDictionary(message: String) -> [String: [String]] {
    let usersMentioned: [String] = findmatches(in: message, patter: "\\(([a-z0-9A-Z]{1,15})\\)", leftInset: 1, rightInset: 1)
    return ["emoticons": usersMentioned]
}

func buildurlsDictionary(message: String) -> [String: [[String:String]]] {
    let usersMentioned: [String] = findmatches(in: message, patter: "https?://([a-z]{2,3}.|)([a-zA-Z0-9_-]+).([a-zA-Z]{2,3})(/[^\\s\\r\\n]+)?", leftInset: 0, rightInset: 0)
    let urldix = usersMentioned.map({["ur":$0]})
    return ["links": urldix ]
}

/// fast try area
let message = "sasdhttp://www.youtube.co/sdsd?=32 https://google.com"
buildurlsDictionary(message: message)
print(buildurlsDictionary(message: message))
let regex = try? NSRegularExpression(pattern: "https?://([a-z]{2,3}.|)([a-zA-Z0-9_-]+).([a-zA-Z]{2,3})(/[^\\s\\r\\n]+)?", options: NSRegularExpression.Options.caseInsensitive)
let matches = regex?.matches(in: message, options: [], range: NSRange(location: 0, length: message.characters.count))
var usersMentioned: [String] = []
for match in matches! {
    let range =  match.rangeAt(0)
    let tr = message.index(message.startIndex, offsetBy: range.location ) ..< message.index(message.startIndex, offsetBy: range.location + range.length )
    usersMentioned.append(message.substring(with: tr))
}
usersMentioned

////

// first test
func oneMentionCase() {
    let str = "@chris®you around?"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    assert(dictionary == ["mentions": [ "chris" ]],"Not correct")
}
// Second test
func emptyStringCase() {
    let str = ""
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    assert(dictionary == ["mentions": []],"Not correct")
}
// Case three
func onlyATsign() {
    let str = "2323@"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    assert(dictionary == ["mentions": []],"Not correct")
}
// Case 4
func nomentionSignAtAll() {
    let str = "83263r7vdevdfyw"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    assert(dictionary == ["mentions": []],"Not correct")
}
// Case 5
func nameAtTheEndOfTheString() {
    let str = "I can't find @chris"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    assert(dictionary == ["mentions": ["chris"]],"Not correct")
}
// Case 6
func whenAtsignFollwedByNonAlphaNumericValue() {
    let str = "I can't find @ chris"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    assert(dictionary == ["mentions": []],"Not correct")
}
// Case 7
func multipleAtSignsFollowedByEachOther() {
    let str = "@@@@@@@@"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    assert(dictionary == ["mentions": []],"Not correct")
}

// Case 8
func multipleAtSignsWithNonAlphaNumericValues() {
    let str = "@@ @ @@ ® @@ @"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    assert(dictionary == ["mentions": []],"Not correct")
}
// Case 9
func multipleAtSignsWithOneMention() {
    let str = "@@ @ @@ ® @@malik @"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    assert(dictionary == ["mentions": ["malik"]],"Not correct")
}
func twoMentionCase() {
    let str = "is @chris or @mat around you"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    
    assert(dictionary == ["mentions": [ "chris", "mat" ]],"Not correct")
}
func threeMentions() {
    let str = "is @chris or @mat or @anyone around you"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    
    assert(dictionary == ["mentions": [ "chris", "mat", "anyone" ]],"Not correct")
}
func stackedMentions() {
    let str = "@chris@mat@sam"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    
    assert(dictionary == ["mentions": [ "chris", "mat", "sam" ]],"Not correct")
}
func creepyCase() {
    let str = "isˆ¨©©•¶bjodjsd|Dskdi09u89b_=sd @ch rwqj0yis or @mat or @anyone around you"
    let dictionary: [String: [String]] = makeemotionsjson(from: str)
    
    assert(dictionary == ["mentions": [ "ch", "mat", "anyone" ]],"Not correct")
}
func testSingleEmoticon() {
    let str = "(hi)"
    let dictionary: [String: [String]] = buildEmoticonsDictionary(message: str)
    
    assert(dictionary == ["emoticons": [ "hi"]],"Not correct")
}
func testMultipleEmoticon() {
    let str = "(hi)(fly)(car)"
    let dictionary: [String: [String]] = buildEmoticonsDictionary(message: str)
    
    assert(dictionary == ["emoticons": [ "hi","fly","car"]],"Not correct")
}
func testBrokenEmoticon() {
    let str = "(hi)(fly(car)"
    let dictionary: [String: [String]] = buildEmoticonsDictionary(message: str)
    
    assert(dictionary == ["emoticons": [ "hi","car"]],"Not correct")
}
func testDeepEmoticon() {
    let str = "(((hi)))"
    let dictionary: [String: [String]] = buildEmoticonsDictionary(message: str)
    
    assert(dictionary == ["emoticons": [ "hi"]],"Not correct")
}
func testMixedEmoticonsWithMentions() {
    let str = "(hi)(fly(car)@sam"
    let dictionary: [String: [String]] = buildEmoticonsDictionary(message: str)
    let mentions: [String: [String]] = buildMentionsDictionary(message: str)
    let all = mentions + dictionary
    assert(all == ["emoticons": [ "hi","car"], "mentions": ["sam"]],"Not correct")
}

/* Run Tests */
oneMentionCase()
emptyStringCase()
onlyATsign()
nomentionSignAtAll()
nameAtTheEndOfTheString()
whenAtsignFollwedByNonAlphaNumericValue()
multipleAtSignsFollowedByEachOther()
multipleAtSignsWithNonAlphaNumericValues()
multipleAtSignsWithOneMention()
twoMentionCase()
threeMentions()
stackedMentions()
creepyCase()
testSingleEmoticon()
testMultipleEmoticon()
testBrokenEmoticon()
testDeepEmoticon()
testMixedEmoticonsWithMentions()
