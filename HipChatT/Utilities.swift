//
//  Utilities.swift
//  HipChatT
//
//  Created by Moath_Othman on 12/2/16.
//  Copyright © 2016 PixilApps. All rights reserved.
//

import Foundation


func == (lhs: StringsArray.Response, rhs: StringsArray.Response) -> Bool {
    var notequal = false
    guard let lhs = lhs , let rhs = rhs else { return notequal }
    notequal = lhs.values.count != rhs.values.count || lhs.keys.count != rhs.keys.count
    let lkeys: [String] = lhs.keys.map {$0}
    let rkeys: [String] = rhs.keys.map {$0}
    if lkeys.count == 0 { return false }
    notequal = lkeys.sorted() != rkeys.sorted()
    let lvalues: [String] = lhs[lkeys.first!]!
    let rvalues: [String] = rhs[lkeys.first!]!
    notequal = lvalues.sorted() != rvalues.sorted()
    return !notequal
}

func + (lhs: StringsArray.Response, rhs: StringsArray.Response) -> StringsArray.Response {
    guard let rkeys: [String] = rhs?.keys.map({$0}) else {
        return nil
    }
    var newDictionary = lhs
    rkeys.forEach {newDictionary![$0] = rhs?[$0]}
    return newDictionary
}

func + (lhs: StringsArray.Response, rhs: DictionaryArray.Response) -> [String: Any]? {
    guard let rkeys: [String] = rhs?.keys.map({$0}) else {
        return nil
    }
    var newDictionary: [String: Any] = lhs!
    rkeys.forEach {newDictionary[$0] = rhs?[$0] }
    return newDictionary
}

