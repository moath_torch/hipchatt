//
//  EmotionsAPI.swift
//  HipChatT
//
//  Created by Moath_Othman on 12/1/16.
//  Copyright © 2016 PixilApps. All rights reserved.
//

import Foundation

// the expected Response
protocol ResponseType {
    associatedtype Response
}
struct StringsArray: ResponseType {
    typealias Response = [String: [String]]?
}
struct DictionaryArray: ResponseType {
    typealias Response = [String: [[String:String]]]?
}
/// The type of the builder (emoji,emoticon,mentions..)
protocol JSONBuilder {
    associatedtype Response
    static func build(message: String) -> Response?
}
/// the chat JSON maker, takes type of builder and response type return the response based on the type
struct ChatJSONMaker<T: JSONBuilder, R: ResponseType> {
    func make(from message: String) -> R.Response {
        return T.build(message: message) as! R.Response
    }
}
/// find @mention
struct MentionsBuilder: JSONBuilder {
    typealias Response = StringsArray.Response

    static func build(message: String) -> Response? {
        // first valid mention @ sign is a sign that is followed by an alphanumeric value
        let regex = RegexMatcher(message: message, pattern: "@[a-z0-9A-Z]+", leftInset: 1, rightInset: 0)
        switch regex.findmatches() {
        case .value(let results):
            return ["mentions": results]
        case .error(let msg):
            print("error \(msg)")
            return nil
        }
    }
}
/// find (emoticon)
struct EmoticonsBuilder: JSONBuilder {
    typealias Response = StringsArray.Response
    
    static func build(message: String) -> Response? {
        let regex = RegexMatcher(message: message, pattern: "\\(([a-z0-9A-Z]{1,15})\\)", leftInset: 1, rightInset: 1)
        switch regex.findmatches() {
        case .value(let results):
            return ["emoticons": results]
        case .error(let msg):
            print("error \(msg)")
            return nil
        }
    }
    
}
/// find URL Links
struct LinksBuilder: JSONBuilder {
    typealias Response = DictionaryArray.Response
    static func build(message: String) -> Response? {
        let regex = RegexMatcher(message: message, pattern: "https?://([a-z]{2,3}.|)([a-zA-Z0-9_-]+).([a-zA-Z]{2,3})(/[^\\s\\r\\n]+)?", leftInset: 0, rightInset: 0)
        switch regex.findmatches() {
        case .value(let results):
            return ["links": results.map({["url":$0]})]
        case .error(let msg):
            print("error \(msg)")
            return nil
        }
    }
    
}


struct RegexMatcher {
    
    enum Result {
        case value([String])
        case error(String)
    }
    
    let message: String
    let pattern: String
    let leftInset: Int
    let rightInset: Int
    
    func findmatches() -> Result {
        guard let regex = try? NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive) else {
            return .error("Pattern is invalid")
        }
        let matches = regex.matches(in: message, options: [], range: NSRange(location: 0, length: message.characters.count))
        var results: [String] = []
        for match in matches {
            let range =  match.rangeAt(0)
            let tr = message.index(message.startIndex, offsetBy: range.location + leftInset) ..< message.index(message.startIndex, offsetBy: range.location + range.length - rightInset)
            results.append(message.substring(with: tr))
        }
        return .value(results)
    }
}

