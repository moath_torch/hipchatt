//
//  ViewController.swift
//  HipChatT
//
//  Created by Moath_Othman on 11/28/16.
//  Copyright © 2016 PixilApps. All rights reserved.
//

import UIKit
import Kanna

class ViewController: UIViewController {

    @IBOutlet weak var resultTextView: UITextView!
    @IBOutlet weak var chatField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showjson(_ sender: Any) {
        
        let linksmaker = ChatJSONMaker<LinksBuilder,DictionaryArray>()
        let links = linksmaker.make(from: self.chatField.text!)
        let json = constructMentionsAndEmoticons() + links
        self.showResults(text: String(describing: json!))
        makeLinksTitled(links: links)
    }
    
    func showResults(text: String) {
        self.resultTextView.text = text
    }
    
    private func constructMentionsAndEmoticons() -> [String: [String]] {
        let mentionsmaker = ChatJSONMaker<MentionsBuilder,StringsArray>()
        let mentions = mentionsmaker.make(from: self.chatField.text!)
        let emoticonsMaker = ChatJSONMaker<EmoticonsBuilder,StringsArray>()
        let emoticons = emoticonsMaker.make(from: self.chatField.text!)
        return (mentions! + emoticons!)!
    }
    
    private func makeLinksTitled(links: DictionaryArray.Response)  {
        guard let links = links?["links"]  else { return }
        var tempLinks = links
        let operatoinQueue = OperationQueue()
        operatoinQueue.maxConcurrentOperationCount = 1
        operatoinQueue.name = "Fetch titles"
        let operation = BlockOperation {
            for (i,dic) in links.enumerated() {
                let urlValue = dic["url"]
                // make a page source
                if let myURL = URL(string: urlValue!) {
                    if let title = Kanna.HTML(url: myURL, encoding: String.Encoding.utf8)?.title{
                        tempLinks[i]["title"] = title
                    }
                }
            }
            let titled = ["links":tempLinks]
            let json = self.constructMentionsAndEmoticons() + titled
            OperationQueue.main.addOperation {
                self.showResults(text: String(describing: json!))
            }
        }
        operatoinQueue.addOperation(operation)
        
    }
}

