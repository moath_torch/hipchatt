//
//  Mentions.swift
//  HipChatT
//
//  Created by Moath_Othman on 12/1/16.
//

import XCTest


class Mentions: XCTestCase {
    let maker = ChatJSONMaker<MentionsBuilder,StringsArray>()
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // first test
    func testoneMentionCase() {
        let str = "@chris®you around?"
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": [ "chris" ]],"Not correct")
    }
    // Second test
    func testemptyStringCase() {
        let str = ""
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": []],"Not correct")
    }
    // Case three
    func testonlyATsign() {
        let str = "2323@"
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": []],"Not correct")
    }
    // Case 4
    func testnomentionSignAtAll() {
        let str = "83263r7vdevdfyw"
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": []],"Not correct")
    }
    // Case 5
    func testnameAtTheEndOfTheString() {
        let str = "I can't find @chris"
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": ["chris"]],"Not correct")
    }
    // Case 6
    func testwhenAtsignFollwedByNonAlphaNumericValue() {
        let str = "I can't find @ chris"
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": []],"Not correct")
    }
    // Case 7
    func testmultipleAtSignsFollowedByEachOther() {
        let str = "@@@@@@@@"
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": []],"Not correct")
    }
    
    // Case 8
    func testmultipleAtSignsWithNonAlphaNumericValues() {
        let str = "@@ @ @@ ® @@ @"
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": []],"Not correct")
    }
    // Case 9
    func testmultipleAtSignsWithOneMention() {
        let str = "@@ @ @@ ® @@malik @"
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": ["malik"]],"Not correct")
    }
    func testtwoMentionCase() {
        let str = "is @chris or @mat around you"
        let dictionary: [String: [String]] = maker.make(from: str)!
        XCTAssert(dictionary == ["mentions": [ "chris", "mat" ]],"Not correct")
    }
    func testthreeMentions() {
        let str = "is @chris or @mat or @anyone around you"
        let dictionary: [String: [String]] = maker.make(from: str)!
        
        XCTAssert(dictionary == ["mentions": [ "chris", "mat", "anyone" ]],"Not correct")
    }
    func teststackedMentions() {
        let str = "@chris@mat@sam"
        let dictionary: [String: [String]] = maker.make(from: str)!
        
        XCTAssert(dictionary == ["mentions": [ "chris", "mat", "sam" ]],"Not correct")
    }
    func testcreepyCase() {
        let str = "isˆ¨©©•¶bjodjsd|Dskdi09u89b_=sd @ch rwqj0yis or @mat or @anyone around you"
        let dictionary: [String: [String]] = maker.make(from: str)!
        
        XCTAssert(dictionary == ["mentions": [ "ch", "mat", "anyone" ]],"Not correct")
    }

    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            self.testcreepyCase()
            // Put the code you want to measure the time of here.
        }
    }
    
}
