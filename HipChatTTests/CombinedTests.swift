//
//  CombinedTests.swift
//  HipChatT
//
//  Created by Moath_Othman on 12/2/16.
//  Copyright © 2016 PixilApps. All rights reserved.
//

import XCTest

class CombinedTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let first = ["1":["2"]]
        let sec = ["2":["3","4"]]
        XCTAssert(first + sec == ["1":["2"], "2": ["3","4"]])
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testcombineASwithDS() {
        let first: [String: [String]]? = ["1":["2"]]
        let sec: [String: [[String:String]]]? = ["2":[["3":"4"]]]
        let r = first + sec
        print(r!)
        XCTAssert(true)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
