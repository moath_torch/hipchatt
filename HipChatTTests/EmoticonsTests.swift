//
//  EmoticonsTests.swift
//  HipChatT
//
//  Created by Moath_Othman on 12/1/16.
//  Copyright © 2016 PixilApps. All rights reserved.
//

import XCTest

class EmoticonsTests: XCTestCase {
    let maker = ChatJSONMaker<EmoticonsBuilder,StringsArray>()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
        
    func testSingleEmoticon() {
        let str = "(hi)"
        let dictionary: [String: [String]] = maker.make(from: str)!
        
        XCTAssert(dictionary == ["emoticons": [ "hi"]],"Not correct")
    }
    
    func testMultipleEmoticon() {
        let str = "(hi)(fly)(car)"
        let dictionary: [String: [String]] = maker.make(from: str)!
        
        XCTAssert(dictionary == ["emoticons": [ "hi","fly","car"]],"Not correct")
    }
    
    func testBrokenEmoticon() {
        let str = "(hi)(fly(car)))()"
        let dictionary: [String: [String]] = maker.make(from: str)!
        
        XCTAssert(dictionary == ["emoticons": [ "hi","car"]],"Not correct")
    }
    
    func testDeepEmoticon() {
        let str = "(((hi)))"
        let dictionary: [String: [String]] = maker.make(from: str)!
        
        XCTAssert(dictionary == ["emoticons": [ "hi"]],"Not correct")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            self.testBrokenEmoticon()
            // Put the code you want to measure the time of here.
        }
    }
    
}
